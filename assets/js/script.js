// // // create array
// // // use an array literal - array literal []


// // const array1 = ["eat", "sleep"];
// // console.log[array1];

// // const  array2 = new Array ('pray', 'play');
// // console.log(array2);

// // // empty array
// // const myList = [];

// // // array of numbers
// // const numArray =[2, 3, 4, 5];

// // // array of strings
// // const stringArray = ['eat', 'work', 'pray', 'play'];

// // //  array of mixed
// // const newData = ['work', 1, true];

// // const newData1 = [
// // 	{'task1': 'exercise'},
// // 	[1,2,3],
// // 	function hello(){
// // 		console.log('Hi I am Array');
// // 	}
// // ];
// // console.log(newData1);

// const placetoVisit = ['boracay', 'south korea', 'davao', 'japan', 'puerto princesa', 'dubai', 'taiwan']
// // console.log(placetoVisit[0]);
// // console.log(placetoVisit);
// // console.log(placetoVisit[placetoVisit.length-1]);
// // console.log(placetoVisit.length);


// // for (let i = 0; i < placetoVisit.length - 1; i++) {
// // 	console.log(placetoVisit[i]);
// // }

// // Array manipulation
// // Add element to an array - push()

// let dailyActivities =['eat', 'work', 'pray', 'play'];
// dailyActivities.push('exercise');
// console.log(dailyActivities);
// // unshift()  add element at the beginning of the  array
// dailyActivities.unshift('sleep');
// console.log(dailyActivities);

// dailyActivities[2] = 'sing';
// console.log(dailyActivities);

// dailyActivities[6] = 'dance';
// console.log(dailyActivities);

// // re-assign the values/items in an array;
// placetoVisit[3] = 'italy';
// console.log(placetoVisit);
// console.log(placetoVisit[5]);

// placetoVisit[5] = 'Rome';
// console.log(placetoVisit);
// console.log(placetoVisit[5]);

// placetoVisit[0] = "Rizal";
// console.log(placetoVisit);

// placetoVisit[6] = 'Catholic HS';
// console.log(placetoVisit);

// let array = [];
// console.log(array[0]);
// array[0] = 'Cloud strife';
// console.log(array);
// console.log(array[1]);
// array[1] = 'Tifa Lockhart';
// console.log(array[1]);
// array[array.length-1] = 'Aerith Gainsborough';
// console.log(array);
// array[array.length] = 'Vincent Valentine';
// console.log(array);

// // array method
// 	// Manipulate array with pre-determine JS function
// 	// Mutators - these arraysmethos usually change the original
// 	// array

// 	let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
// 	// without method
// 	array1[array.length] = 'Francisco';
// 	console.log(array1);

// 	// push() - alows us  to add an element at the end
// 	array1.push('Andresw');
// 	console.log(array1);

// 	// .unshift() - allows us to add an element at the beginning
// 	// of the array
// 	array1.unshift('Simon');
// 	console.log(array1);

// // .pop () - allow us to delete or remove the array
// // at the end  of the array
// array1.pop();
// console.log(array1);

// // .pop() - is also  able to return the item we removed
// console.log(array1.pop());
// console.log(array1);

// let removedItem = array1.pop();
// console.log(array1);
// console.log(removedItem);

// // .shift() return the item we removed
// let removedItemShift = array1.shift();
// console.log(array1);
// console.log(removedItemShift);

// // array1.unshift('George');
// // console.log(array1);

// // array1.push('Michael');
// // console.log(array1);

// // // .sort() - by default, will allow us to sort our items
// // // items in ascending order.

// // array1.sort();
// // console.log(array1);

// // let numArray = [3, 2, 1, 6, 7, 9];
// // numArray.sort();
// // console.log(numArray);

// // let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
// // numArray2.sort((a,b) => a-b);/*anonymousfunction or shortcut function (a-b)  => a-b*/
// // console.log(numArray2);
// // // .sor() converts all items into  strings and then arrange
// // // the items accordingly as  if they are words/text.

// // // ascending sort order
// // numArray2.sort(function(a,b){
// // 	return a-b
// // })
// // console.log(numArray2);

// // // descending sort order
// // numArray2.sort(function(a,b){
// // 	return b-a
// // })
// // console.log(numArray2);

// // let arrayStr = ['Marie', 'Zen', 'Jaime', 'Elaine'];
// // arrayStr.sort(function(a,b){
// // 	return b - a
// // })
// // console.log(arrayStr);

// // // .reverse() - reversed the order of the items
// // arrayStr.sort();
// // console.log(arrayStr);
// // arrayStr.sort().reverse();
// // console.log(arrayStr);



// // let beatles = ['George', 'John', 'Paul', 'Ringo'];
// // let lakersPlayer = ['Lebron', 'David', 'Westbrook', 'Kobe', 'Shaq'];
// // // .splice() - allows us to remove and add elements  from a
// // //  given index
// // // index. syntax: array.splice(staringIndex, numberofItemstobeDeleted, elementstoAdd)

// // lakersPlayer.splice(0, 0, 'Caruso');
// // console.log(lakersPlayer);
// // lakersPlayer.splice(0, 1);
// // console.log(lakersPlayer);
// // lakersPlayer.splice(0, 3);
// // console.log(lakersPlayer);
// // lakersPlayer.splice(1, 1);
// // console.log(lakersPlayer);
// // lakersPlayer.splice(1, 0, 'Gasol', 'Fisher');
// // console.log(lakersPlayer);

// // // Non-Mutators
// // 	// Methods that will not change the original
// // 	// slice() - allows us to get a portion of theoriginal array and return a new arraywith the items selected form the original
// // 	// syntax: slice(startIndex, endIndex)

// // let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
// // computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
// // console.log(computerBrands);

// // let newBrands = computerBrands.slice(1,3);
// // console.log(computerBrands);
// // console.log(newBrands);

// let fonts = ['Times New Roman', 
// 'Comic San MS', 
// 'Impact', 
// 'Monotype Corsiva', 
// 'Arial', 
// 'Arial Black'];
// console.log(fonts);

// let newFontSet = fonts.slice(1, 5);
// console.log(newFontSet);

// newFontSet = fonts.slice().reverse();
// console.log(newFontSet);

// // 

// let videoGame = ['PS4', 'PS5', 'Switch',
// 		'Xbox', 'Xbox1'
// ]
// console.log(videoGame);

// let microsoft = videoGame.slice(3)
// console.log(microsoft);

// let nintendo = videoGame.slice(2, 4)
// console.log(nintendo);

// // .toString() - convert the array into a single value
//   // as a string but eac item  will be separated by a comma
// // syntax: array.toString()

// let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];
// let sentenceString = sentence.toString();
// console.log(sentence);
// console.log(sentenceString);

// // .join() - converts the array into a single value as  a string but separator
// // can be specified.

// // syntax: array.join(separator)

// let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
// let sentenceString2 = sentence.join(' ');
// console.log(sentenceString2);

// let sentenceString3 = sentence2.join("/");
// console.log(sentenceString3);


/* 			
			Mini-Activity

			Given a set of characters, 
				-form the name, "Martin" as a single string.
				-form the name, "Miguel" as a single string.
			Use the methods we discussed so far.

			save "Martin" and "Miguel" to variables:
				-name 1 and name 2

			log both variables on the console.

*/

let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

let martinArray = charArr.slice(5,11);
let name2 = martinArray.join('');
console.log(name2);

let MiguelArray = charArr.slice(13,19);
let name3 = MiguelArray.join('');
console.log(name3);

 // -concat() - it combines 2 or more arrays without affectingthe
 // original

 // syntax: array.concat(array1, array2);

 let tasksFriday  = ['drink HTML', 'eat Javascript'];
 let tasksSaturday = ['inhale CSS', 'breath BootStrap'];
 let tasksSunday = ['Get Git', 'Be Node'];

 let weekendTasks = tasksFriday.concat(tasksSaturday, tasksSunday);
 console.log(weekendTasks);

 // Accessors
 	 // Methods that allow us to access our array.
 	 // indexOf()
 	  // - finds the index of the given element/item when it is
 	   // first found from the left

 let batch131 = [
		'Paolo',
		'Jamir',
		'Jed',
		'Ronel',
		'Rom',
		'Jayson' 
];

console.log(batch131.indexOf('Jed'));
console.log(batch131.indexOf('Rom'));

// LastIndexof()
	// - finds the index of the given element/item
	// when it is last found from the right.

console.log(batch131.lastIndexOf('Jamir'));
console.log(batch131.lastIndexOf('Jayson'));

console.log(batch131.lastIndexOf('Kim'));

/*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
*/
		let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];

function indexing(array4, item){
	console.log(array4.indexOf(item));
}
function indexing1(array5, item1){
	console.log(array5.lastIndexOf(item1))
}

indexing(carBrands, 'BMW');
indexing1(carBrands, 'BMW');

// Iterator Methods
// These methos iterate overtheitems in an array much like loop
// However, with our iterator methods there  also that allows
// to not only iterate over items but also additional instruction

let avengers = [
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spider-man',
		'Iron Man',
		'Captain America'
];

// forEach()
	// similar to for loop but  is used on arrays. it will
	// allow us  o iterate over each time in an array.
	// and even add instruction per iteration.
// Anonymous function within forEach will be receive
 // each and every item in an array

 avengers.forEach(function(avenger){
 	console.log(avenger);
 }) 


 let marvelHeroes = [
 		'Moon Knight',
 		'Jessica Jones',
 		'Deadpool',
 		'Cyclops'
 ];
marvelHeroes.forEach(function(hero){
	// iterate over all the items in Marvel Heroes array and let them join the avenger
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// add an if-else wherein cyclops and deadpool is not allow to join
		avengers.push(hero);
	}
})
 console.log(avengers);

 // map() - similar to forEach however it returns new  array

// let number = [25, 50. 30, 5];

// let mappedNumbers = number.map(function(number){
// 	console.log(number);
// 	return number * 5
// }) 
// console.log(mappedNumbers);

// every()
	// iterates over all  the items and checks if all  the 
	// elements passess a given condition 

let allMemberAge = [25, 30, 15, 20, 26];

let checkAlladult = allMemberAge.every(function(age){
	console.log(age);
	return age >=18
});
console.log(checkAlladult); 

// some()
	// iterate over all the items check if even at least
	// one of the items in the aray passes the condition. same as every()
	// it will return boolean.
let examScores =[75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});
console.log(checkForPassing);

// filter() - creates a new array that containes element
// which passed a given condition

let numberArr2 = [500, 12, 120, 60, 6, 30];

let  divisibleBy5 = numberArr2.filter(function(number){
	return number % 5 === 0;
});
console.log(divisibleBy5);

// find()
// - iterate over all items in our array but only return
// the item that will satisfy the given condtion
let registeredUsername = ['pedro101', 'mikeyTheKing2000', 'superPhoenix',
'sheWhoCode'];

let foundUser = registeredUsername.find(function(username){
	console.log(username);
	return username === 'mikeyTheKing2000'; 
});
console.log(foundUser);

// .includes() - returns a boolean true if it finds
//  a matching item in the array.
// Case - sensitive

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michaleKing@gmail.com',
	'pedro_himselft@yahoo.com',
	'sheJonesSmith@gmail.com'
];

let  doesEmailExist = registeredEmails.includes('mic8aleKing@gmail.com');
console.log(doesEmailExist);

/*
	Mini-Activity
	Create 2 functions 
		First Function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second Function is able to find a specified email already exist in the registeredEmails array.
			- IF  there is an email found, show an alert:
				"Email Already Exist."
			- IF there is no email found, show an alert:
				"Email is available, proceed to registration."

		You may use any of the three methods we discussed recently.

*/

function onlinGamer(username){
	let foundUser = registeredUsername.find(function(username){
		console.log(username);
		return username;
	});
}
onlinGamer('sheWhoCode');

function checkEmail(email) {

    let existeMail = registeredEmails.includes(email);
    if (existeMail ==  true){
    	alert("Email Alreadyexist")
    } else alert("email is available, Proceed to registeration")

   };
   checkEmail('sheJonesSmith@gmail.com');

function checkEmail(email) {

    let existeMail = registeredEmails.includes(email);
    if (existeMail ==  true){
    	alert("Email Already exist")
    } else alert("email is available, Proceed to registeration")

   };
   checkEmail('mackoy@gmail.com');





